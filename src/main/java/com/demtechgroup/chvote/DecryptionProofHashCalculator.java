/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import com.demtechgroup.chvote.chport.service.model.Encryption;
import java.math.BigInteger;
import java.util.List;

/**
 * Hash calculation support for decryption proofs - this is built on the original Hash class but targeted more
 * specifically
 * towards the requirements for verifying decryption proofs.
 */
class DecryptionProofHashCalculator
    extends ProofHashCalculator {
  /**
   * Base constructor.
   *
   * @param hashFactory factory to provide HashCalculators for use with this class.
   */
  public DecryptionProofHashCalculator(com.demtechgroup.chvote.HashFactory hashFactory) {
    super(hashFactory);
  }

  /**
   * Calculate a challenge hash for a NIZKP for a shuffle proof.
   *
   * @param pk_j    encryption key share.
   * @param e       ElGamal encryptions.
   * @param b_prime partial decryptions.
   * @param t       the commitments vector.
   *
   * @return the challenge hash, value composed of the arguments.
   */
  byte[] calculateHash(BigInteger pk_j, List<Encryption> e, List<BigInteger> b_prime, List<BigInteger> t) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(recHash_L(pk_j, e, b_prime));
    calculator.update(recHash_L(t));

    return calculator.getResult();
  }

  /**
   * Algorithm 4.9: RecHash_L, non-varargs version
   * <p>
   * Computes the hash value h(v_1, ..., v_k) \in B^L of multiple inputs v_1, ..., v_k in a recursive manner, taking
   * into
   * account the requirements of a decryption proof calculator.
   * </p>
   * <p>
   * This method performs the necessary casts and conversions for the hashing to be compliant to the definition in
   * section 4.3. Note that unlike the ShuffleProofHashCalculator there is specific handling of the Encryption object
   * so that only the B component is added to the hash in progress.
   * </p>
   *
   * @param object the element which needs to be cast
   *
   * @return the recursive hash as defined in section 4.3
   */
  protected byte[] recHash_L(Object object) {
    if (object instanceof BigInteger) {
      return hash_L((BigInteger) object);
    } else if (object instanceof Encryption)   // note only use B value in this case.
    {
      return hash_L(((Encryption) object).getB());
    } else if (object instanceof byte[]) {
      return hash_L((byte[]) object);
    } else if (object instanceof List) {
      return recHash_L(((List) object));
    } else if (object instanceof Object[]) {
      return recHash_L((Object[]) object);
    } else {
      throw new IllegalArgumentException(MessageUtils.getMessage("HashCalculator.unknown_object_error")
                                         + ((object != null) ? object.getClass().getName() : "null"));
    }
  }
}
