/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.chport.service.model;

/**
 * This model class holds the necessary data an entry in the ballot list held by the authorities
 */
public final class BallotEntry {
  private final Integer        i;
  private final BallotAndQuery alpha;

  public BallotEntry(Integer i, BallotAndQuery alpha) {
    this.i = i;
    this.alpha = alpha;
  }

  public Integer getI() {
    return i;
  }

  public BallotAndQuery getAlpha() {
    return alpha;
  }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        BallotEntry that = (BallotEntry) o;
//        return Objects.equals(i, that.i) &&
//                Objects.equals(alpha, that.alpha);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(i, alpha);
//    }
}
