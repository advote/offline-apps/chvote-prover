/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.chport.service.model;

import com.demtechgroup.chvote.chport.service.support.Conversion;
import java.math.BigInteger;

/**
 * This model class holds the value of an encryption public key
 */
public final class EncryptionPublicKey {
  private final BigInteger      publicKey;
  private final EncryptionGroup encryptionGroup;
  private final transient Conversion conversion = new Conversion();

  public EncryptionPublicKey(BigInteger publicKey, EncryptionGroup encryptionGroup) {
    this.publicKey = publicKey;
    this.encryptionGroup = encryptionGroup;
  }

  public BigInteger getPublicKey() {
    return publicKey;
  }

  public EncryptionGroup getEncryptionGroup() {
    return encryptionGroup;
  }
}
