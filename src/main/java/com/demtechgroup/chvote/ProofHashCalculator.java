/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import java.math.BigInteger;
import java.util.List;

public abstract class ProofHashCalculator {
  protected final com.demtechgroup.chvote.HashFactory hashFactory;

  protected ProofHashCalculator(com.demtechgroup.chvote.HashFactory hashFactory) {
    this.hashFactory = hashFactory;
  }

  /**
   * Algorithm 4.9: RecHash_L, varargs version
   * <p>
   * Computes the hash value h(v_1, ..., v_k) \in B^L of multiple inputs v_1, ..., v_k in a recursive manner.
   * </p>
   *
   * @param objects an array of objects to hash
   *
   * @return The recursive hash as defined in section 4.3
   */
  protected byte[] recHash_L(Object... objects) {
    HashCalculator calculator = hashFactory.createCalculator();

    if (objects.length == 1) {
      return recHash_L(objects[0]);
    } else {
      for (Object object : objects) {
        calculator.update(recHash_L(object));
      }
    }

    return calculator.getResult();
  }

  /**
   * Algorithm 4.9: RecHash_L, List version
   * <p>
   * Computes the hash value h(v_1, ..., v_k) \in B^L of multiple inputs v_1, ..., v_k in a recursive manner.
   * </p>
   *
   * @param objects an array of objects to hash
   *
   * @return The recursive hash as defined in section 4.3
   */
  protected byte[] recHash_L(List objects) {
    HashCalculator calculator = hashFactory.createCalculator();

    if (objects.size() == 1) {
      return recHash_L(objects.get(0));
    } else {
      for (Object object : objects) {
        calculator.update(recHash_L(object));
      }
    }

    return calculator.getResult();
  }

  /*
   * Handle a byte array, no offset.
   */
  protected byte[] hash_L(byte[] byteArray) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(byteArray);

    return calculator.getResult();
  }

  /*
   * Handle a byte array, from offset.
   */
  protected byte[] hash_L(byte[] byteArray, int offSet) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(byteArray, offSet, byteArray.length - offSet);

    return calculator.getResult();
  }

  /*
   * Handle a big integer (encoded and hashed as unsigned)
   */
  protected byte[] hash_L(BigInteger integer) {
        /*
         * Algorithm 4.3: ToByteArray
         *
         * @param x the integer to be converted
         * @return the byte array corresponding to the integer
         */
    byte[] bytes = integer.toByteArray();

    if (bytes[0] == 0) {
      return hash_L(bytes, 1);
    } else {
      return hash_L(bytes, 0);
    }
  }

  protected abstract byte[] recHash_L(Object o);
}
