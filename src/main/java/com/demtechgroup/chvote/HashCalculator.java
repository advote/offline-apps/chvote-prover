/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

/**
 * Calculator for hashes.
 */
public interface HashCalculator {
  /**
   * Update the calculator with the byte array bytes.
   *
   * @param bytes a byte array of input data to the calculator.
   */
  void update(byte[] bytes);

  /**
   * Update the calculator with data contained in the byte array bytes.
   *
   * @param bytes  a byte array containing input data to the calculator.
   * @param off    the offset the data starts at in the byte array.
   * @param length the length of the data in the byte array.
   */
  void update(byte[] bytes, int off, int length);

  /**
   * Calculate the hash of all the preceding updates. Resets the calculator.
   *
   * @return the result of processing all updates to date.
   */
  byte[] getResult();
}
