/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import com.demtechgroup.chvote.bc.Blake2BHashFactory;
import com.demtechgroup.chvote.chport.service.model.BallotAndQuery;
import com.demtechgroup.chvote.chport.service.model.BallotEntry;
import com.demtechgroup.chvote.chport.service.model.Confirmation;
import com.demtechgroup.chvote.chport.service.model.ConfirmationEntry;
import com.demtechgroup.chvote.chport.service.model.DecryptionProof;
import com.demtechgroup.chvote.chport.service.model.Encryption;
import com.demtechgroup.chvote.chport.service.model.EncryptionGroup;
import com.demtechgroup.chvote.chport.service.model.EncryptionPublicKey;
import com.demtechgroup.chvote.chport.service.model.IdentificationGroup;
import com.demtechgroup.chvote.chport.service.model.NonInteractiveZKP;
import com.demtechgroup.chvote.chport.service.model.PrimeField;
import com.demtechgroup.chvote.chport.service.model.PublicParameters;
import com.demtechgroup.chvote.chport.service.model.SecurityParameters;
import com.demtechgroup.chvote.chport.service.model.ShuffleProof;
import com.demtechgroup.chvote.json.JSONNode;
import com.demtechgroup.chvote.json.JSONParser;
import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * Root class for the verification process.
 */
public class Verifier {
  private final ExecutorService    threadPool;
  private final List<ProofMessage> messages = new ArrayList<>();
  private List<BigInteger> cachedPrimes;


  /**
   * Base constructor.
   *
   * @param threadPool the thread pool to use for running the various proof tasks.
   */
  public Verifier(ExecutorService threadPool) {
    this.threadPool = threadPool;
  }

  @SuppressWarnings("unchecked")
  public List<ProofMessage> process(String inputName, Reader jsonReader)
      throws IOException {
    messages.add(new ProofMessage(MessageUtils.getMessage("Verifier.file_title") + " " + inputName));
    messages.add(new ProofMessage(MessageUtils.getMessage("Verifier.version_title") + " " + "1.0.0"));

    JSONParser parser = new JSONParser(jsonReader);

    Map<String, Number> secParams = null;
    Map<String, BigInteger> idGroup = null;
    Map<String, BigInteger> prmField = null;

    BigInteger q_hat_x = null;
    BigInteger q_hat_y = null;
    BigInteger upper_l_r = null;
    BigInteger upper_l_f = null;
    BigInteger s = null;
    BigInteger n_max = null;
    BigInteger l_x = null;
    BigInteger l_y = null;
    BigInteger l_r = null;
    BigInteger l_f = null;
    BigInteger upper_l_m = null;
    List<Character> upper_a_x = null;
    List<Character> upper_a_y = null;
    List<Character> upper_a_r = null;
    List<Character> upper_a_f = null;
    EncryptionGroup encryptionGroup = null;
    List<BigInteger> publicKeyShares = null;
    List<DecryptionProof> decryptionProofs = null;
    List<List<Encryption>> encryptions = null;
    List<List<BigInteger>> partialDecryptions = null;
    EncryptionPublicKey electionOfficerKey = null;
    List<ShuffleProof> pi = null;
    List<BigInteger> bold_w = null;
    List<BigInteger> generators = null;
    List<ConfirmationEntry> confirmations = null;
    List<BallotEntry> ballots = null;

    List<BigInteger> numCandidates = new ArrayList<>();

    messages.add(new ProofMessage(Timestamp.getTimestamp() + " " + MessageUtils.getMessage("Verifier.started")));

    JSONNode node;

    while ((node = parser.readNode()) != null) {
      if ("publicParameters".equals(node.getName())) {
        Map<String, Object> params = loadPublicParameters(node);

        secParams = loadSecurityParameters(params);

        encryptionGroup = loadEncryptionGroup((List<JSONNode>) params.get("encryptionGroup"));
        idGroup = loadBigIntegers((List<JSONNode>) params.get("identificationGroup"));
        prmField = loadBigIntegers((List<JSONNode>) params.get("primeField"));

        q_hat_x = (BigInteger) params.get("q_hat_x");
        q_hat_y = (BigInteger) params.get("q_hat_y");
        upper_l_r = (BigInteger) params.get("upper_l_r");
        upper_l_f = (BigInteger) params.get("upper_l_f");
        s = (BigInteger) params.get("s");
        n_max = (BigInteger) params.get("n_max");
        l_x = (BigInteger) params.get("l_x");
        l_y = (BigInteger) params.get("l_y");
        l_r = (BigInteger) params.get("l_r");
        l_f = (BigInteger) params.get("l_f");
        upper_l_m = (BigInteger) params.get("upper_l_m");
        upper_a_x = getCharacterList((List) params.get("upper_a_x"));
        upper_a_y = getCharacterList((List) params.get("upper_a_y"));
        upper_a_r = getCharacterList((List) params.get("upper_a_r"));
        upper_a_f = getCharacterList((List) params.get("upper_a_f"));
      } else if ("electionSet".equals(node.getName())) {
        List<JSONNode> nodeList = (List<JSONNode>) node.getValue();
        int totalCandidateCount = 0;
        for (JSONNode elt : nodeList) {
          if (elt.getName().equals("elections")) {
            List<JSONNode> elList = (List<JSONNode>) elt.getValue();

            for (JSONNode election : elList) {
              numCandidates.add((BigInteger) ((List<JSONNode>) election.getValue())
                  .parallelStream().filter(n -> n.getName().equals("numberOfCandidates"))
                  .findFirst().map(JSONNode::getValue).orElse(BigInteger.ZERO));
            }
          } else if (elt.getName().equals("candidates")) {
            List<JSONNode> elList = (List<JSONNode>) elt.getValue();
            totalCandidateCount = elList.size();
          } else if (elt.getName().equals("bold_w")) {
            bold_w = toBigIntegers(elt);
          }
        }

        if (totalCandidateCount != getTotal(numCandidates)) {
          throw new IllegalStateException(MessageUtils.getMessage("Verifier.candidate_count_error"));
        }
      } else if ("electionOfficerPublicKey".equals(node.getName())) {
        electionOfficerKey = loadElectionOfficerPublicKey(node, encryptionGroup);
      } else if ("ballots".equals(node.getName())) {
        ballots = loadBallots(node);
      } else if ("confirmations".equals(node.getName())) {
        confirmations = loadConfirmations(node);
      } else if ("decryptionProofs".equals(node.getName())) {
        decryptionProofs = loadDecryptionProofs(node);
      } else if ("partialDecryptions".equals(node.getName())) {
        partialDecryptions = loadPartialDecryptions(node);
      } else if ("publicKeyParts".equals(node.getName())) {
        publicKeyShares = loadPublicKeyParts(node, encryptionGroup);
      } else if ("shuffles".equals(node.getName())) {
        encryptions = loadShuffles(node);
      } else if ("shuffleProofs".equals(node.getName())) {
        pi = loadShuffleProofs(node);
      } else if ("primes".equals(node.getName())) {
        cachedPrimes = toBigIntegers(node);
      } else if ("generators".equals(node.getName())) {
        generators = toBigIntegers(node);
      }
    }

    if (encryptionGroup == null
        || electionOfficerKey == null
        || ballots == null
        || confirmations == null
        || decryptionProofs == null
        || partialDecryptions == null
        || publicKeyShares == null
        || encryptions == null
        || pi == null
        || cachedPrimes == null
        || generators == null) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.file_not_expected_format"));
    }

    SecurityParameters securityParameters = new SecurityParameters(
        secParams.get("sigma").intValue(),
        secParams.get("tau").intValue(),
        secParams.get("upper_l").intValue(),
        secParams.get("epsilon").doubleValue());
    IdentificationGroup identificationGroup = new IdentificationGroup(
        idGroup.get("p_hat"),
        idGroup.get("q_hat"),
        idGroup.get("g_hat"));
    PrimeField primeField = new PrimeField(prmField.get("p_prime"));

    //populatePrimesCache(encryptionGroup, getTotal(numCandidates) + getMax(bold_w) + 1);

    boolean passed = true;
    byte[] fullDigest = null;

    messages.add(
        new ProofMessage(Timestamp.getTimestamp() + " " + MessageUtils.getMessage("Verifier.started_verification")));

    PublicParameters publicParameters = new PublicParameters(securityParameters, encryptionGroup,
                                                             identificationGroup, primeField, q_hat_x,
                                                             upper_a_x, q_hat_y, upper_a_y, upper_a_r,
                                                             upper_l_r.intValue(), upper_a_f,
                                                             upper_l_f.intValue(), s.intValue(), n_max.intValue());

    // if we don't have a pre-defined thread pool we'll guess based on the number of proofs.
    ExecutorService localThreadPool;
    if (threadPool != null) {
      localThreadPool = threadPool;
    } else {
      localThreadPool = Executors.newFixedThreadPool(publicParameters.getS() + 1);
    }

    //HashFactory hashFactory = new JcaHashFactory("BLAKE2B-256", "BC", publicParameters.getSecurityParameters());
    HashFactory hashFactory = new Blake2BHashFactory(256, publicParameters.getSecurityParameters());
    DecryptionProofChecker ta =
        new DecryptionProofChecker(new ArgumentValidator(publicParameters), hashFactory, localThreadPool);

    // The election officer key is also a key share to allow the officials control of the results
    List<BigInteger> keyShares = new ArrayList<>();

    keyShares.addAll(publicKeyShares);
    keyShares.add(electionOfficerKey.getPublicKey());

    Future<ProofResult> decFuture = ta.checkDecryptionProofs(decryptionProofs,
                                                             keyShares, encryptions.get(encryptions.size() - 1),
                                                             partialDecryptions);

    ShuffleProofChecker sa =
        new ShuffleProofChecker(new ArgumentValidator(publicParameters), hashFactory, localThreadPool);

    List<Encryption> e0 = getEncryptions(encryptionGroup, ballots, confirmations, numCandidates, bold_w);

    List<Future<ProofResult>> shuffleResults = sa.checkShuffleProofs(pi,
                                                                     e0,
                                                                     encryptions,
                                                                     getPublicKey(keyShares, encryptionGroup),
                                                                     generators);

    HashCalculator hCalc = hashFactory.createCalculator();

    try {
      ProofResult proofResult = decFuture.get();

      messages.add(proofResult);

      if (proofResult.isOkay()) {
        hCalc.update(proofResult.getHash());
      } else {
        passed = false;
      }

      for (Future<ProofResult> result : shuffleResults) {
        proofResult = result.get();

        messages.add(proofResult);

        if (proofResult.isOkay()) {
          hCalc.update(proofResult.getHash());
        } else {
          passed = false;
        }
      }
    } catch (InterruptedException e) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.task_interrupted_error"), e);
    } catch (ExecutionException e) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.task_execution_error"), e);
    }

    fullDigest = hCalc.getResult();

    if (passed) {
      messages.add(new ProofResult(Timestamp.getTimestamp() + " "
                                   + MessageUtils.getMessage("Verifier.finished"), fullDigest));
    } else {
      messages.add(new ProofResult(Timestamp.getTimestamp() + " " + MessageUtils.getMessage("Verifier.finished")));
    }

    if (localThreadPool != threadPool) {
      localThreadPool.shutdown();
    }

    return messages;
  }

  @SuppressWarnings("unchecked")
  private Map<String, Number> loadSecurityParameters(Map<String, Object> params) {
    Map<String, Number> secParams = new HashMap<>();

    List<JSONNode> nodeList = (List<JSONNode>) params.get("securityParameters");
    if (nodeList != null) {
      for (JSONNode elt : nodeList) {
        secParams.put(elt.getName(), elt.getValue());
      }
    }

    return secParams;
  }

  private Map<String, Object> loadPublicParameters(JSONNode node) {
    Map<String, Object> params = new HashMap<>();

    for (JSONNode n : node.<List<JSONNode>>getValue()) {
      if (n.getType() == JSONNode.Type.STRING) {
        params.put(n.getName(), n.getValueAs(JSONNode.Type.INTEGER));
      } else {
        params.put(n.getName(), n.getValue());
      }
    }
    return params;
  }

  private EncryptionGroup loadEncryptionGroup(List<JSONNode> nodeList) {
    Map<String, BigInteger> encGroup = loadBigIntegers(nodeList);
    return new EncryptionGroup(
        encGroup.get("p"),
        encGroup.get("q"),
        encGroup.get("g"),
        encGroup.get("h"));
  }

  private Map<String, BigInteger> loadBigIntegers(List<JSONNode> nodeList) {
    Map<String, BigInteger> group = new HashMap<>();

    if (nodeList != null) {
      for (JSONNode elt : nodeList) {
        group.put(elt.getName(), elt.getValueAs(JSONNode.Type.INTEGER));
      }
    }

    return group;
  }

  private EncryptionPublicKey loadElectionOfficerPublicKey(JSONNode node, EncryptionGroup encryptionGroup) {
    List<JSONNode> nodeList = node.getValue();

    JSONNode publicKey = nodeList.get(1);

    if (!"publicKey".equals(publicKey.getName())) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.public_key_expected_error"));
    }

    checkEncryptionGroup(nodeList.get(0), encryptionGroup);

    return new EncryptionPublicKey(publicKey.getValueAs(JSONNode.Type.INTEGER), encryptionGroup);
  }

  private List<List<BigInteger>> loadPartialDecryptions(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<List<BigInteger>> partialDecryptions = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> decList = elt.getValue();
      partialDecryptions.add(toBigIntegers(decList.get(0)));
    }

    return partialDecryptions;
  }

  private List<BallotEntry> loadBallots(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<BallotEntry> ballots = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> parts = elt.getValue();
      BigInteger x_hat = parts.get(2).getValueAs(JSONNode.Type.INTEGER);

      List<BigInteger[]> a = new ArrayList<>();
      for (JSONNode lr : parts.get(0).<List<JSONNode>>getValue()) {
        List<JSONNode> lrL = lr.getValue();
        BigInteger left = lrL.get(0).getValueAs(JSONNode.Type.INTEGER);
        BigInteger right = lrL.get(1).getValueAs(JSONNode.Type.INTEGER);

        a.add(new BigInteger[]{left, right});
      }
      List<JSONNode> piN = parts.get(1).getValue();

      ballots.add(new BallotEntry(
          Integer.parseInt(elt.getName()),
          new BallotAndQuery(x_hat, a,
                             new NonInteractiveZKP(toBigIntegers(piN.get(0)), toBigIntegers(piN.get(1))))));
    }

    return ballots;
  }

  private List<ConfirmationEntry> loadConfirmations(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<ConfirmationEntry> confirmations = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> entry = elt.getValue();
      List<JSONNode> piN = entry.get(0).getValue();

      confirmations.add(
          new ConfirmationEntry(Integer.parseInt(elt.getName()),
                                new Confirmation(entry.get(1).getValueAs(JSONNode.Type.INTEGER),
                                                 new NonInteractiveZKP(toBigIntegers(piN.get(1)),
                                                                       toBigIntegers(piN.get(0))))));
    }

    return confirmations;
  }

  private List<BigInteger> loadPublicKeyParts(JSONNode node, EncryptionGroup encryptionGroup) {
    List<JSONNode> nodeList = node.getValue();
    List<BigInteger> publicKeyShares = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> nl = elt.getValue();

      JSONNode publicKeyShare = nl.get(1);

      if (!"publicKey".equals(publicKeyShare.getName())) {
        throw new IllegalStateException(MessageUtils.getMessage("Verifier.public_key_expected_error"));
      }

      checkEncryptionGroup(nl.get(0), encryptionGroup);

      publicKeyShares.add(publicKeyShare.getValueAs(JSONNode.Type.INTEGER));
    }

    return publicKeyShares;
  }

  private List<ShuffleProof> loadShuffleProofs(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<ShuffleProof> pi = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> proofN = elt.getValue();
      List<JSONNode> tN = proofN.get(3).getValue();
      BigInteger t_1 = tN.get(0).getValueAs(JSONNode.Type.INTEGER);
      BigInteger t_2 = tN.get(1).getValueAs(JSONNode.Type.INTEGER);
      BigInteger t_3 = tN.get(2).getValueAs(JSONNode.Type.INTEGER);

      List<JSONNode> sN = proofN.get(2).getValue();
      BigInteger s_1 = sN.get(0).getValueAs(JSONNode.Type.INTEGER);
      BigInteger s_2 = sN.get(1).getValueAs(JSONNode.Type.INTEGER);
      BigInteger s_3 = sN.get(2).getValueAs(JSONNode.Type.INTEGER);
      BigInteger s_4 = sN.get(3).getValueAs(JSONNode.Type.INTEGER);

      ShuffleProof proof = new ShuffleProof(
          new ShuffleProof.T(t_1, t_2, t_3, toBigIntegers(tN.get(3)), toBigIntegers(tN.get(4))),
          new ShuffleProof.S(s_1, s_2, s_3, s_4, toBigIntegers(sN.get(4)), toBigIntegers(sN.get(5))),
          toBigIntegers(proofN.get(0)), toBigIntegers(proofN.get(1)));

      pi.add(proof);
    }

    return pi;
  }

  private List<DecryptionProof> loadDecryptionProofs(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<DecryptionProof> decryptionProofs = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> nl = elt.getValue();

      JSONNode t = nl.get(1);
      JSONNode s = nl.get(0);

      if (!"t".equals(t.getName())) {
        throw new IllegalStateException(MessageUtils.getMessage("Verifier.t_expected_error"));
      }
      if (!"s".equals(s.getName())) {
        throw new IllegalStateException(MessageUtils.getMessage("Verifier.s_expected_error"));
      }

      decryptionProofs.add(new DecryptionProof(toBigIntegers(t), (BigInteger) s.getValueAs(JSONNode.Type.INTEGER)));
    }

    return decryptionProofs;
  }

  private List<List<Encryption>> loadShuffles(JSONNode node) {
    List<JSONNode> nodeList = node.getValue();
    List<List<Encryption>> encryptions = new ArrayList<>(nodeList.size());

    for (JSONNode elt : nodeList) {
      List<JSONNode> nl = elt.getValue();
      List<Encryption> encs = new ArrayList<>();

      for (JSONNode n : nl) {
        List<JSONNode> ab = n.getValue();

        JSONNode a = ab.get(0);
        if (!"a".equals(a.getName())) {
          throw new IllegalStateException(MessageUtils.getMessage("Verifier.a_expected_error"));
        }
        JSONNode b = ab.get(1);
        if (!"b".equals(b.getName())) {
          throw new IllegalStateException(MessageUtils.getMessage("Verifier.b_expected_error"));
        }

        encs.add(new Encryption(a.getValueAs(JSONNode.Type.INTEGER),
                                b.getValueAs(JSONNode.Type.INTEGER)));
      }

      encryptions.add(encs);
    }

    return encryptions;
  }

  private void checkEncryptionGroup(JSONNode node, EncryptionGroup encryptionGroup) {
    if (!"encryptionGroup".equals(node.getName())) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.enc_group_expected_error"));
    }

    EncryptionGroup localEncGroup = loadEncryptionGroup(node.getValue());

    if (!encryptionGroup.getP().equals(localEncGroup.getP())
        || !encryptionGroup.getQ().equals(localEncGroup.getQ())
        || !encryptionGroup.getG().equals(localEncGroup.getG())
        || !encryptionGroup.getH().equals(localEncGroup.getH())) {
      throw new IllegalStateException(MessageUtils.getMessage("Verifier.enc_group_mismatch_error"));
    }
  }

  private List<Character> getCharacterList(List<JSONNode> list) {
    List<Character> rv = new ArrayList<>(list.size());
    for (int i = 0; i != rv.size(); i++) {
      rv.add(list.get(i).getValue());
    }
    return rv;
  }

  private List<BigInteger> toBigIntegers(JSONNode node) {

    List<JSONNode> nodeList = node.getValue();
    BigInteger[] rv;

    rv = new BigInteger[nodeList.size()];
    for (int i = 0; i != rv.length; i++) {
      rv[i] = nodeList.get(i).getValueAs(JSONNode.Type.INTEGER);
    }

    return Arrays.asList(rv);
  }

  private int getMax(List<BigInteger> ints) {
    int max = 0;

    for (BigInteger i : ints) {
      int v = i.intValue();
      if (v > max) {
        max = v;
      }
    }

    return max;
  }

  private int getTotal(List<BigInteger> ints) {
    int total = 0;

    for (BigInteger i : ints) {
      total += i.intValue();

    }

    return total;
  }

  /**
   * Algorithm 7.34: HasConfirmation (list of ConfirmationEntry replaced with set of indexes).
   *
   * @param i         the voter index
   * @param upper_c_I the set of confirmation indexes.
   *
   * @return true if the list of confirmation contains a confirmation for the given voter index, false otherwise
   */
  public boolean hasConfirmation(Integer i, Set<Integer> upper_c_I) {
    return upper_c_I.contains(i);
  }

  /**
   * Algorithm 7.1: GetPrimes
   * <p>
   * Note: these values are used for permuting ballots in the system - the code here must
   * be identical to what is used in the voting system in order to make sure the numbers come out
   * the same way. The first shuffle proof from initial state may fail otherwise.
   * </p>
   *
   * @param n the number of requested primes
   *
   * @return the ordered list of the n first primes found in the group
   */
  public List<BigInteger> getPrimes(int n) {
    if (cachedPrimes.size() < n) {
      throw new IllegalStateException("The primes cache should have been populated beforehand");
    }
    return cachedPrimes.subList(0, n);
  }

  /**
   * Algorithm 7.39: GetEncryptions
   *
   * @param upper_b the list of ballots submitted to the bulletin board
   * @param upper_c the list of confirmations submitted to the bulletin board
   * @param bold_n  the number of candidates per election
   * @param bold_w  the indices of the counting circles
   *
   * @return the list of the encryptions for the valid, confirmed ballots, "coloured" by counting circle
   */
  public List<Encryption> getEncryptions(EncryptionGroup encryptionGroup,
                                         Collection<BallotEntry> upper_b,
                                         List<ConfirmationEntry> upper_c,
                                         List<BigInteger> bold_n,
                                         List<BigInteger> bold_w) {
    BigInteger p = encryptionGroup.getP();
    int n = getTotal(bold_n);
    int w = getMax(bold_w);
    List<BigInteger> primes;

    primes = getPrimes(n + w + 1);

    Set<Integer> confirmationIndexes = new HashSet<>();

    for (ConfirmationEntry c : upper_c) {
      confirmationIndexes.add(c.getI());
    }

    List<Encryption> enc = new ArrayList<>(upper_b.size());
    for (BallotEntry ballotEntry : upper_b) {
      if (hasConfirmation(ballotEntry.getI(), confirmationIndexes)) {
        BigInteger p_w_i = primes.get(n + bold_w.get(ballotEntry.getI()).intValue());
        BigInteger a_1 = p_w_i;
        BigInteger a_2 = BigInteger.ONE;
        for (BigInteger[] ints : ballotEntry.getAlpha().getBold_a()) {
          a_1 = a_1.multiply(ints[0]).mod(p);
          a_2 = a_2.multiply(ints[1]).mod(p);
        }

        enc.add(new Encryption(a_1, a_2));
      }
    }

    Collections.sort(enc, new Comparator<Encryption>() {
      @Override
      public int compare(Encryption o1, Encryption o2) {
        int rv = o1.getA().compareTo(o2.getA());
        if (rv != 0) {
          return rv;
        }
        return o1.getB().compareTo(o2.getB());
      }
    });

    return enc;
  }

  /**
   * Algorithm 7.16: GetPublicKey (encryption group is extra parameter).
   *
   * @param publicKeyShares the set of public key shares that should be combined
   * @param encryptionGroup group details associated with the public values
   *
   * @return the combined public key
   */
  public EncryptionPublicKey getPublicKey(List<BigInteger> publicKeyShares, EncryptionGroup encryptionGroup) {
    BigInteger publicKey = BigInteger.ONE;

    for (BigInteger keyShare : publicKeyShares) {
      publicKey = publicKey.multiply(keyShare).mod(encryptionGroup.getP());
    }
    return new EncryptionPublicKey(publicKey, encryptionGroup);
  }
}
