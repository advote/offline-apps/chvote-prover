/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.json;

import java.math.BigInteger;
import java.util.Base64;

/**
 * Basic node for use with parser.
 */
public class JSONNode {
  public enum Type {
    ARRAY,
    BODY,
    DECIMAL,
    INTEGER,
    NULL,
    STRING
  }

  private final Type   type;
  private final String name;
  private final Object value;

  public JSONNode(Type type, Object value) {
    this.type = type;
    this.name = null;
    this.value = value;
  }

  public JSONNode(Type type, String name, Object value) {
    this.type = type;
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public Type getType() {
    return type;
  }

  @SuppressWarnings("unchecked")
  public <T> T getValue() {
    return (T)value;
  }

  @SuppressWarnings("unchecked")
  public <T> T getValueAs(Type asType) {
    if (this.type == asType) {
      return (T)value;
    }

    if (this.type == Type.STRING && asType == Type.INTEGER) {
      return (T)new BigInteger(Base64.getDecoder().decode((String) value));
    }

    throw new IllegalStateException("unknown conversion requested");
  }
}
