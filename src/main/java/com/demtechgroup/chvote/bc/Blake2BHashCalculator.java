/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.bc;

import com.demtechgroup.chvote.HashCalculator;
import java.util.Arrays;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.Blake2bDigest;

/**
 * Calculator for Blake2B hashes.
 */
public class Blake2BHashCalculator
    implements HashCalculator {
  private final Digest digest;
  private final int    upper_l;

  /**
   * Base constructor.
   *
   * @param bitStrength bit size of digest to use.
   * @param upper_l     strength of the calculated result (bytes).
   */
  Blake2BHashCalculator(int bitStrength, int upper_l) {
    this.digest = new Blake2bDigest(bitStrength);
    this.upper_l = upper_l;
  }

  /**
   * Return the length of the underlying digest.
   *
   * @return the maximum length of the calculated hash.
   */
  int getDigestLength() {
    return digest.getDigestSize();
  }

  /**
   * Update the calculator with the byte array bytes.
   *
   * @param bytes a byte array of input data to the calculator.
   */
  public void update(byte[] bytes) {
    digest.update(bytes, 0, bytes.length);
  }

  /**
   * Update the calculator with data contained in the byte array bytes.
   *
   * @param bytes  a byte array containing input data to the calculator.
   * @param off    the offset the data starts at in the byte array.
   * @param length the length of the data in the byte array.
   */
  public void update(byte[] bytes, int off, int length) {
    digest.update(bytes, off, length);
  }

  /**
   * Calculate the hash of all the preceding updates. Resets the calculator.
   *
   * @return the result of processing all updates to date.
   */
  public byte[] getResult() {
    byte[] out = new byte[digest.getDigestSize()];

    digest.doFinal(out, 0);

    return truncate(out, upper_l);
  }

  private static byte[] truncate(byte[] a, int length) {
    return Arrays.copyOfRange(a, 0, length);
  }
}
