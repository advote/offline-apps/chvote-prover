/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.test;

import com.demtechgroup.chvote.ArgumentValidator;
import com.demtechgroup.chvote.DecryptionProofChecker;
import com.demtechgroup.chvote.chport.service.model.DecryptionProof;
import com.demtechgroup.chvote.chport.service.model.Encryption;
import com.demtechgroup.chvote.chport.service.model.EncryptionGroup;
import com.demtechgroup.chvote.chport.service.model.IdentificationGroup;
import com.demtechgroup.chvote.chport.service.model.PrimeField;
import com.demtechgroup.chvote.chport.service.model.PublicParameters;
import com.demtechgroup.chvote.chport.service.model.SecurityParameters;
import com.demtechgroup.chvote.jcajce.JcaHashFactory;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import org.junit.Assert;
import org.junit.Test;

public class ProofCheckerTest {
  @Test
  public void shouldPassDecryptionData()
      throws Exception {
    ObjectInputStream oIn = new ObjectInputStream(getClass().getResourceAsStream("/enc_proof_test.data"));

    List<DecryptionProof> decryptionProofs = new ArrayList<>();
    int decSize = oIn.readInt();
    for (int i = 0; i != decSize; i++) {
      BigInteger s = (BigInteger) oIn.readObject();

      @SuppressWarnings("unchecked")
      List<BigInteger> t = (List<BigInteger>) oIn.readObject();

      decryptionProofs.add(new DecryptionProof(t, s));
    }

    @SuppressWarnings("unchecked")
    List<BigInteger> publicKeyShares = (List<BigInteger>) oIn.readObject();
    List<Encryption> encryptions = new ArrayList<>();
    int encSize = oIn.readInt();
    for (int i = 0; i != encSize; i++) {
      BigInteger a = (BigInteger) oIn.readObject();
      BigInteger b = (BigInteger) oIn.readObject();

      encryptions.add(new Encryption(a, b));
    }

    @SuppressWarnings("unchecked")
    List<List<BigInteger>> partialDecryptions = (List<List<BigInteger>>) oIn.readObject();

    PublicParameters publicParameters = getTestPublicParameters();

    com.demtechgroup.chvote.HashFactory hashFactory =
        new JcaHashFactory("SHA-512", "SUN", publicParameters.getSecurityParameters());

    DecryptionProofChecker proofChecker =
        new DecryptionProofChecker(new ArgumentValidator(getTestPublicParameters()), hashFactory,
                                   Executors.newSingleThreadExecutor());

    Assert.assertTrue(
        proofChecker.checkDecryptionProofs(decryptionProofs, publicKeyShares, encryptions, partialDecryptions).get()
                    .isOkay());
  }

  private PublicParameters getTestPublicParameters() {
    return new PublicParameters(
        new SecurityParameters(80, 80, 20, 0.999),
        getTestEncryptionGroup(),
        getTestIdentificationGroup(),
        getTestPrimeField(),
        new BigInteger("730750818665451459101842416358141509827966271787"),
        Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
                      'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                      'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
        ),
        new BigInteger("730750818665451459101842416358141509827966271787"),
        Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
                      'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                      'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
        ),
        Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
                      'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                      'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
        ),
        2,
        Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                      'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
                      'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                      'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
        ),
        2,
        3,
        1678);
  }

  private PrimeField getTestPrimeField() {
    return new PrimeField(new BigInteger("882206846675319814556984837240935671829850975383"));
  }

  private EncryptionGroup getTestEncryptionGroup() {
    return new EncryptionGroup(
        new BigInteger(
            "89884656743115795386465259539451236680898848947115328636715040578866337902750481566354238661203768010560056939935696678829394884407208311246423715319737062188883946712432742638151109800623047059726541476042502884419075341171231440736956555270413618581675255342293149119973622969239858152417678164812113740223"),
        new BigInteger(
            "44942328371557897693232629769725618340449424473557664318357520289433168951375240783177119330601884005280028469967848339414697442203604155623211857659868531094441973356216371319075554900311523529863270738021251442209537670585615720368478277635206809290837627671146574559986811484619929076208839082406056870111"),
        new BigInteger(
            "62001487766852373844016962725711002207168742839990053828809946801165305152095204790659589623010479418570215065695938701158344382198189391686686700494727889979715598687908280018086953295785971247054291131827973689571952998726745996720254009432722435558345308921773432579883357891903994897937057510630204481307"),
        new BigInteger(
            "63307628119750370013600456634799789180959219054969887946783363816571172289297828177056346641856916919866018171593366308011291490941048949734072226568153339324029395870198787031737609156318370787430473397289077395837793531171600129235052950342261414992261059783939858577834086795186075762869392175921174352669")
    );
  }

  private IdentificationGroup getTestIdentificationGroup() {
    return new IdentificationGroup(
        new BigInteger(
            "89884656743115795386465259539451236680898848947115328636715040578866337902750481566354238661203768010560056939935696678829394884407208311246423715319737062188883946712432742638151109800623047059726541476042502884419075341171231440736956555270413618581675255529365358698328774708775703215219351545329613875969"),
        new BigInteger("730750818665451459101842416358141509827966271787"),
        new BigInteger(
            "43753966268956158683794141044609048074944399463497118601009260015278907944793396888872654797436679156171704835263342098747229841982963550871557447683404359446377648645751856913829280577934384831381295103182368037001170314531189658120206052644043469275562473160989451140877931368137440524162645073654512304068"));
  }
}
