/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.test;

import com.demtechgroup.chvote.MessageUtils;
import com.demtechgroup.chvote.ProofMessage;
import com.demtechgroup.chvote.ProofResult;
import com.demtechgroup.chvote.Verifier;
import java.io.InputStreamReader;
import java.security.Security;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

public class ProverTest {
  @Test
  public void shouldParseJSON()
      throws Exception {
    Security.addProvider(new BouncyCastleProvider());

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    Verifier prover = new Verifier(threadPool);

    List<ProofMessage> messages = prover.process("verificationData-2048-SIMPLE_SAMPLE-250.json",
                                                 new InputStreamReader(getClass().getResourceAsStream(
                                                     "/verificationData-2048-SIMPLE_SAMPLE-250.json")));

    for (ProofMessage message : messages) {
      System.out.println(message);
      if (message instanceof ProofResult) {
        Assert.assertTrue(((ProofResult) message).isOkay());
      }
    }

    threadPool.shutdown();
  }

  @Test
  public void shouldFailOnInconsistentShuffle()
      throws Exception {
    Security.addProvider(new BouncyCastleProvider());

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    Verifier prover = new Verifier(threadPool);

    try {
      List<ProofMessage> messages = prover.process("verificationData-2048-SIMPLE_SAMPLE-100_inc_shuffle.json",
                                                   new InputStreamReader(getClass().getResourceAsStream(
                                                       "/verificationData-2048-SIMPLE_SAMPLE-100_inc_shuffle.json")));
      Assert.fail("no exception");
    } catch (IllegalArgumentException e) {
      Assert.assertEquals(MessageUtils.getMessage("ShuffleProofChecker.encryption_list_count_error"), e.getMessage());
    }

    threadPool.shutdown();
  }

  @Test
  public void shouldFailOnInconsistentShuffleProof()
      throws Exception {
    Security.addProvider(new BouncyCastleProvider());

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    Verifier prover = new Verifier(threadPool);

    try {
      List<ProofMessage> messages = prover.process("verificationData-2048-SIMPLE_SAMPLE-100_inc_shuffle_proof.json",
                                                   new InputStreamReader(getClass().getResourceAsStream(
                                                       "/verificationData-2048-SIMPLE_SAMPLE-100_inc_shuffle_proof.json")));
      Assert.fail("no exception");
    } catch (IllegalArgumentException e) {
      Assert.assertEquals(MessageUtils.getMessage("ShuffleProofChecker.pi_count_error"), e.getMessage());
    }

    threadPool.shutdown();
  }

  @Test
  public void shouldFailOnInconsistentDecryptionProof()
      throws Exception {
    Security.addProvider(new BouncyCastleProvider());

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    Verifier prover = new Verifier(threadPool);

    try {
      List<ProofMessage> messages = prover.process("verificationData-2048-SIMPLE_SAMPLE-100_inc_dec_proof.json",
                                                   new InputStreamReader(getClass().getResourceAsStream(
                                                       "/verificationData-2048-SIMPLE_SAMPLE-100_inc_dec_proof.json")));
      Assert.fail("no exception");
    } catch (IllegalArgumentException e) {
      Assert.assertEquals(MessageUtils.getMessage("DecryptionProofChecker.proof_count_wrong_error"), e.getMessage());
    }

    threadPool.shutdown();
  }

  @Test
  public void shouldFailOnInconsistentAuthorities()
      throws Exception {
    Security.addProvider(new BouncyCastleProvider());

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    Verifier prover = new Verifier(threadPool);

    try {
      List<ProofMessage> messages = prover.process("verificationData-2048-SIMPLE_SAMPLE-100_inc_authority.json",
                                                   new InputStreamReader(getClass().getResourceAsStream(
                                                       "/verificationData-2048-SIMPLE_SAMPLE-100_inc_authority.json")));
      Assert.fail("no exception");
    } catch (IllegalArgumentException e) {
      Assert.assertEquals(MessageUtils.getMessage("DecryptionProofChecker.proof_count_wrong_error"), e.getMessage());
    }

    threadPool.shutdown();
  }
}
